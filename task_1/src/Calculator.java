import java.text.DecimalFormat;
import java.util.Stack;
import java.lang.String;
import java.util.ArrayList;
import java.lang.StringBuilder;
import java.lang.Math;
/**
 * Created by Kravchenko Vlad on 04.10.2016.
 * cheshirekrab@gmail.com
 */
public class Calculator {

    public Calculator(){}

    //A stack of operands
    private Stack<Double> operandStack = new Stack<Double>();

    //A stack of operators (+-*/)
    private Stack<String> operatorStack = new Stack<String>();

    // The possible operators
    private final String OPERATORS = "()+-/*";
    private final String NONBRACES = "+-/*";
    //                           (  )  +  -  /  *
    private final int[] ORDER = {0, 0, 1, 1, 2, 2};
    //An ArrayList of every single stand-alone item
    ArrayList<String> input = new ArrayList<String>();
    //An empty ArrayList to return null upon error
    ArrayList<String> fail = new ArrayList<String>();

    DecimalFormat df = new DecimalFormat("#.####");

    /**
     * Stripping a user-input string to an every single item
     * @param string - user input string
     * @return input - the correct ArrayList
     * @return fail - empty ArrayList(null) in case of an error
     */

    public ArrayList inputCleaner(String string){
        StringBuilder sb = new StringBuilder();
        String noSpaces = string.replace(" ", "");
        for (int i = 0; i < noSpaces.length(); i++) {
            char c = noSpaces.charAt(i);
            boolean isNum = (c >= '0' && c <= '9');

            if (isNum) {
                sb.append(c);
                if (i == noSpaces.length()-1) {
                    input.add(sb.toString());
                    sb.delete(0, sb.length());
                }
            } else if (c == '.') {
                for (int j = 0; j < sb.length(); j++) {
                    if (sb.charAt(j) == '.') {
                        //You can't have two decimals in a number
                        return fail;
                    } else if (j == sb.length() - 1) {
                        sb.append(c);
                        j = (sb.length() + 1);
                    }
                }
                if (sb.length() == 0) {
                    sb.append(c);
                }
                if (i == noSpaces.length()-1) {
                    //You can't end your equation with a decimal
                    return fail;
                }
            } else if (OPERATORS.indexOf(c)!= -1) {
                if (sb.length() != 0) {
                    input.add(sb.toString());
                    sb.delete(0, sb.length());
                }
                sb.append(c);
                input.add(sb.toString());
                sb.delete(0, sb.length());
            } else {
                //Make sure your input only contains numbers, correct operators, or parantheses
                return fail;
            }
        }

        int numLP = 0;
        int numRP = 0;

        for (int f = 0; f < input.size(); f++) {
            String OnlyP = input.get(f);

            switch (OnlyP) {
                case "(": numLP++;
                    break;
                case ")": numRP++;
                    break;
                default: //do nothing
                    break;
            }

        }
        if (numLP != numRP) {
            //The number of brackets don't match up
            return fail;
        }

        int orderP = 0;
        for (int f = 0; f < input.size(); f++) {
            String awesome = input.get(f);
            switch (awesome) {
                case "(": orderP++;
                    break;
                case ")": orderP--;
                    break;
                default: //do nothing
                    break;
            }
            if (orderP < 0) {
                //The order of your parentheses is off
                return fail;
            }
        }
        if (NONBRACES.indexOf(input.get(input.size()-1)) != -1) {
            //The input can't end with an operator
            return fail;
        }
        return input;
    }

    /**
     * Method to process operators
     * @param op - operator
     */
    private void processOperator(String op)  {
        if (operatorStack.empty() || op.equals("(")) {
            operatorStack.push(op);
        } else {
            //peek the operator stack and
            //let topOp be the top operator.
            String topOp = operatorStack.peek();
            if (precedence(op) > precedence(topOp)) {
                topOp = op;
                operatorStack.push(op);
            } else {
                //Pop all stacked operators with equal
                // or higher precedence than op.
                while (operandStack.size() >= 2 && !operatorStack.isEmpty()) {
                    double r = operandStack.pop();
                    double l = operandStack.pop();
                    String work = getNextNonBracerOperator();

                    doOperandWork(work, l, r);

                    if(op.equals("(")) {
                        //matching '(' popped - exit loop.
                        operandStack.push(l);
                        operandStack.push(r);
                        break;
                    }

                    if (!operatorStack.empty()) {
                        //reset topOp
                        topOp = operatorStack.peek();
                    }
                }
                //assert: Operator stack is empty or
                // current operator order > top of stack operator order.
                if(!op.equals(")") ) {
                    operatorStack.push(op);
                }
            }
        }
    }

    /**
     * Process he input string
     * @param expressions - the result of the inputCleaner()
     * @return final answer
     */
    public String process(ArrayList<String> expressions){
        for (String expression : expressions) {
            if (OPERATORS.indexOf(expression) == -1) {
                operandStack.push(Double.parseDouble(expression));
            } else {
                processOperator(expression);
            }
        }
        while (operandStack.size() >= 2 && !operatorStack.isEmpty()) {
            double r = operandStack.pop();
            double l = operandStack.pop();
            String work = getNextNonBracerOperator();

            doOperandWork(work, l, r);
        }
        if(operandStack.isEmpty())
            return null;
        double val = operandStack.pop();//This will round the answer saving the '.' instead of ','
        val = val*10000;
        val = Math.round(val);
        val = val /10000;
        return String.valueOf(val);
    }

    /**
     * goes through the stack and pops off all non operatable operations until it gets to one that is in the NONBRACES String
     * @return The next operatable string
     */
    private String getNextNonBracerOperator() {
        String work = "\0";
        while(!operatorStack.isEmpty() && NONBRACES.indexOf(work) == -1)
            work = operatorStack.pop();
        return work;
    }

    /**
     * @param work The operator you want to work
     * @param l Left side number
     * @param r Right side number
     */
    private void doOperandWork(String work, double l, double r){
        switch (work) {
            case "+": operandStack.push(l+r);
                break;
            case "-": operandStack.push(l-r);
                break;
            case "*": operandStack.push(l*r);
                break;
            case "/": operandStack.push(l/r);
                break;
        }
    }

    /**
     * @param op operator
     * @return the order
     */
    private int precedence(String op) {
        return ORDER[OPERATORS.indexOf(op)];
    }

}
