import java.lang.String;
import java.util.ArrayList;
/**
 * Created by Kravchenko Vlad on 04.10.2016.
 * cheshirekrab@gmail.com
 *
 * Calculator
 * Написать калькулятор для вычисления простейших арифметических выражений. Арифметическим выражением считается выражение, включающее:
 * 1. Цифры
 * 2. Точка в качестве десятичного разделителя
 * 3. Круглые скобки
 * 4. Знаки операций («+», «-», «*», «/»)
 * Приоритет операций: скобки, умножение-деление, сумма-вычитание.
 * Округление производится до 4-го знака после запятой, округляется только конечный результат.
 * В качестве входного параметра в метод передается строка с арифметическим выражением
 * В результате ожидается строка с вычисленным значением либо null, если в выражение не может быть вычислено.
 */
public class Main {
    public static void main(String[] args) {
        Calculator calc = new Calculator();
        ArrayList test = new ArrayList();
        //Scanner s = new Scanner(System.in);

        //System.out.println("Please insert an argument: ");

        //String g = s.nextLine();
        //String g = "(1+1)*(3+1)-(2/2)";
        String g = "((1*1)+(3*1)-(2/2))/3 + 1/3";
        test = calc.inputCleaner(g);
        /*
        for (int z = 0; z < test.size(); z++) {
            System.out.println(test.get(z));
        }
        */
        System.out.println(calc.process(test));
    }
}
