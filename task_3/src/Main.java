import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created by Kravchenko Vlad on 04.10.2016.
 * cheshirekrab@gmail.com
 *
 * Duplicates
 * Задается входной файл, содержащий текстовые строки.
 * Программа создает в указанном месте выходной файл, содержащий отсортированные по алфавиту неповторяющиеся строки исходного файла.
 * конце каждой строки в квадратных скобках указывается количество повторений данной строки во входном файле.
 * В качестве входных параметра в метод передаются два файла: первый – входной, второй – выходной.
 * Метод возвращает true тогда и только тогда, когда обработка файла прошла успешно.
 * В случае возникновения ошибок программа должна вернуть false.
 * Не гарантируется, что данные файлы существуют.
 * В случае, если выходной файл не существует, он должен быть создан.
 * Если он существует, необходимо дописать результат выполнения программы, без перезаписи уже содержащейся там информации.
 */
public class Main {
    public static void main(String[] args) throws Exception {
        String inputFile = "input.in";
        String outputFile = "output.out";
/*
        Scanner s = new Scanner(System.in);

        System.out.println("Please insert an input file path: ");
        String inputFile = s.nextLine();
        System.out.println("Please insert an output file path: ");
        String outputFile = s.nextLine();
*/

        Path path = Paths.get(outputFile);
        if (!Files.exists(path)){
            Files.createFile(path);
        }
        Duplicates dup = new Duplicates();
        System.out.println(dup.process(inputFile, outputFile));
    }
}
