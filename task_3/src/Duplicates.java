import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

/**
 * Created by Kravchenko Vlad on 04.10.2016.
 * cheshirekrab@gmail.com
 */
public class Duplicates {
public Duplicates() {
}
/**
 * Implementing the sort and find algorithm
 * @param input - string containig an input file path
 * @param output - string containig an output file path
 * @return true in case everything went well
 * @return false in case the input file path is invalid
 */
public boolean process(String input, String output) throws Exception{
    try{
        Path path = Paths.get(input); //Check if there are input and output files. If not - create empty ones
        if (!Files.exists(path)) {
            Files.createFile(path);
        }
        path = Paths.get(output);
        if (!Files.exists(path)) {
            Files.createFile(path);
        }

        FileReader fileReader = new FileReader(input);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        String inputLine;
        List<String> lineList = new ArrayList<String>(); //Put the lines from the file into an ArrayList
        while ((inputLine = bufferedReader.readLine()) != null) {
            lineList.add(inputLine);
        }
        fileReader.close();

        Collections.sort(lineList);

        TreeMap<String, Integer> repeatMap = new TreeMap<String, Integer>(); //Put the sorted lines from the ArrayList into a TreeMap to order the alphabetically
        for (String str : lineList) {

            if (repeatMap.containsKey(str)) {
                repeatMap.put(str, repeatMap.get(str) + 1);
            } else {
                repeatMap.put(str, 1);
            }
        }

        FileWriter fileWriter = new FileWriter(output, true); //Print the entries of the TreeMap into a file without overwriting
        PrintWriter out = new PrintWriter(fileWriter);
        for (Map.Entry<String, Integer> entry : repeatMap.entrySet()) {
            String outputLine = entry.getKey() + " [" + entry.getValue() + "] ";
            System.out.println(outputLine);
            out.println(outputLine);
        }

        out.flush();
        out.close();
        fileWriter.close();
        return true;
    }catch(IOException e){
        return false;
    }
}
}

