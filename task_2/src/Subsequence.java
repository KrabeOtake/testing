import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

/**
 * Created by Kravchenko Vlad on 04.10.2016.
 * cheshirekrab@gmail.com
 *
 * Subsequence
 * Заданы две последовательности X1, X2,..., Xn и Y1, Y2,..., Yk произвольных элементов  (java.lang.Object).
 * Определить,  можно  ли  получить последовательность X путем вычеркивания некоторых элементов из Y?
 * В качестве входных параметра в метод передаются два списка: первый – список Xi, второй – список Yi.
 */
public class Subsequence {
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);

        System.out.println("Enter the length of the first sequence: ");
        int n = reader.nextInt();
        ArrayList<Object> objects1 = new ArrayList<>(n);
        for (int i = 0; i < n; i++)
            objects1.add(new Integer(i));

        System.out.println("Enter the length of the second sequence: ");
        n = reader.nextInt();
        ArrayList<Object> objects2 = new ArrayList<>(n);
        for (int i = 0; i < n; i++)
            objects2.add(new Integer(i));

        Iterator selector_1 = objects1.iterator();
        Iterator selector_2 = objects2.iterator();

        System.out.println("");
        System.out.println("The first sequence: ");
        while (selector_1.hasNext()) {
            System.out.print(selector_1.next() + " ");
        }

        System.out.println("");
        System.out.println("");

        System.out.println("The second sequence: ");
        while (selector_2.hasNext()) {
            System.out.print(selector_2.next() + " ");
        }

        System.out.println("");
        System.out.println("");

        FindSub findsub = new FindSub();
        System.out.println(findsub.find(objects1, objects2));

    }
}
