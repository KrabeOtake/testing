import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by Kravchenko Vlad on 04.10.2016.
 * cheshirekrab@gmail.com
 */
public class FindSub {

    private ArrayList sequence_f, sequence_s;

    public FindSub(){}

    public boolean find(ArrayList seq1, ArrayList seq2){
        sequence_f = new ArrayList(seq1);
        sequence_s = new ArrayList(seq2);
        if(sequence_f.size() > sequence_s.size()){
            return false; // No chances of finding a subsequence in a smaller array
        }
        else{
            Iterator selector_1 = sequence_f.iterator();
            Iterator selector_2 = sequence_s.iterator();
            Object temp1 = selector_1.next();
            Object temp2 = selector_2.next();

            int i = 0;
            while(selector_2.hasNext()){
                if(temp1.equals(temp2)){
                    i++;
                    if(selector_1.hasNext()) temp1 = selector_1.next();
                    temp2 = selector_2.next();
                }
                else {
                    temp2 = selector_2.next();
                }
            }
            return (i == sequence_f.size())? true : false;
        }
    }

}
